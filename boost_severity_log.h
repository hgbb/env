/*
 * my_boost_log.h
 *
 *  Created on: Sep 22, 2016
 *      Author: hong
 */

#ifndef BOOST_SEVERITY_LOG_H_
#define BOOST_SEVERITY_LOG_H_

#include <boost/log/attributes.hpp>
#include <boost/log/common.hpp>
#include <boost/log/core.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/filter_parser.hpp>
#include <boost/log/utility/setup/formatter_parser.hpp>
#include <boost/log/utility/setup/from_stream.hpp>
#include <fstream>

BOOST_LOG_INLINE_GLOBAL_LOGGER_INIT(_boost_severity_logger,
                                    boost::log::sources::severity_logger<>) {
  boost::log::add_common_attributes();
  boost::log::core::get()->add_global_attribute(
      "Scope", boost::log::attributes::named_scope());
  try {
#ifndef PATH_TO_BOOST_LOG_SETTING_INI
#define PATH_TO_BOOST_LOG_SETTING_INI "boost_log_setting.ini"
#endif
    std::ifstream f(PATH_TO_BOOST_LOG_SETTING_INI);
    boost::log::init_from_stream(f);
  } catch (...) {
    boost::log::add_console_log();
  }

  return {};
}

#define LOG_TRACE                              \
  BOOST_LOG_SEV(_boost_severity_logger::get(), \
                boost::log::trivial::severity_level::trace)
#define LOG_DEBUG                              \
  BOOST_LOG_SEV(_boost_severity_logger::get(), \
                boost::log::trivial::severity_level::debug)
#define LOG_INFO                               \
  BOOST_LOG_SEV(_boost_severity_logger::get(), \
                boost::log::trivial::severity_level::info)
#define LOG_WARN                               \
  BOOST_LOG_SEV(_boost_severity_logger::get(), \
                boost::log::trivial::severity_level::warning)
#define LOG_ERROR                              \
  BOOST_LOG_SEV(_boost_severity_logger::get(), \
                boost::log::trivial::severity_level::error)
#define LOG_FATAL                              \
  BOOST_LOG_SEV(_boost_severity_logger::get(), \
                boost::log::trivial::severity_level::fatal)

#define LOG_SCOPE(scope) BOOST_LOG_SCOPED_THREAD_TAG("Scope", scope)
#define LOG_TAG(scope) BOOST_LOG_SCOPED_THREAD_TAG("Tag", scope)
#define LOG_SCOPE_FUNC BOOST_LOG_FUNC()
#define LOG_SCOPE_FUNCTION BOOST_LOG_FUNCTION()
#define LOG_FUNC LOG_SCOPE_FUNC
#define LOG_FUNCTION LOG_SCOPE_FUNCTION

#endif /* BOOST_SEVERITY_LOG_H_ */
