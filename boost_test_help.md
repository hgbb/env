
BOOST TEST FRAMEWORK
主要是用来做单元测试，而不是作为库或者产品代码，这些地方使用 `assert()`, `boost::concept_check` 或者 `BOOST_STATIC_ASSERT()` 检测和报告更合适 。
	

#用词解释

##测试模块（Test Module）
分为 **单文件测试模块模块（single-file test module）** 和 **多文件测试模块（multi-file test module）**

每个 **测试模块** 包含四个部分
1. test setup (or test initialization)
2. test body
3. test cleanup
4. test runner

其中 **test runner** 是可选的，如果一个测试模块被构建为可执行程序，测 **test runner** 已被内置其中。如果一个测试模块被构建为动态库，则它被一个
 external test runner 运行。
 
##Test body
这是测试模块执行真正测试逻辑的地方。逻辑上 **test body** 是一个封装了 **test assertions** ，被组织成 **test tree** 的测试用例集合。

##Test tree（测试树）
    
	master_test_suite
	├── test_suite1
	│   ├── test_case11
	│   ├── test_case12
	│   ├── test_suilte11
	│   │   ├── test_case111
	│   │   ├── test_case112
	│   │   └── test_case113
	│   └── test_suilte12
	│       └── test_case121
	└── test_suite2
	    └── test_case21
    


##Test unit（测试单元）
一个 test suites 或者 test cases 的集合的统称

##Test case（测试用例）
一个独立的被检测函数，可保护一个或多个 **test assertions** .未捕获的异常或者其它普通的测试用例不会导致测试终止。事实上测试用例造成的错误会被观测器捕获并被报告，然后接着执行下一个测试用例。最好写个多个小测试用例而不是写一个大的函数。 

##Test suite（测试套件）
主要用于给测试用例分组。这样做有以下几个好处：
* 给每个子系统的用例分组
* 共享 test case setup/cleanup 代码
* 选择某些用例组
* 测试报告可以根据分组拆分
* 根据一些测试树上的测试单元的测试结果跳过某些测试用例 



##Test setup
保护以下几个阶段：
* 测试框架启动欧冠你
* 测试树构建
* 全局测试模块构建
* 每个测试用例构建

##Test cleanup
##Test fixture
将频繁使用的 setup 和 cleanup 的操作组合而成的实体。

##Test runner
##Test log
##Test report

#三种使用方法
##独立头文件（不需要链接库，编译慢）
	#define BOOST_TEST_MODULE test module name
	#include <boost/test/included/unit_test.hpp>

##静态库(推荐)
	#define BOOST_TEST_MODULE test module name
	#include <boost/test/unit_test.hpp>

##动态库	（磁盘占用小）
	#define BOOST_TEST_MODULE test module name
	#define BOOST_TEST_DYN_LINK
	#include <boost/test/unit_test.hpp>	

** BOOST_TEST_MODULE ** 在一个文件定义一次即可  
默认库名称 ** boost_unit_test_framework **


#编写测试单元
##断言等级
* ``WARN``	失败不会增加用例错误数
* ``CHECK``	失败会继续执行
* ``REQUIRE``	失败会中断当前测试用例

##用法

	BOOST_AUTO_TEST_CASE(test_case_name);
	BOOST_AUTO_TEST_CASE(test_case, *decor1() *decor2())
	BOOST_FIXTURE_TEST_CASE(test_case_name, fixture_name);
	BOOST_FIXTURE_TEST_CASE(test_case, Fx, *decor1() *decor2())
	BOOST_DATA_TEST_CASE(test_case_name, dataset) { /* dataset1 of arity 1 */ }
	BOOST_DATA_TEST_CASE(test_case_name, dataset, var1) { /* datasets of arity 1 */ }
	BOOST_DATA_TEST_CASE(test_case_name, dataset, var1, ..., varN) { /* datasets of arity N  */ }	
	BOOST_DATA_TEST_CASE_F(fixture, test_case_name, dataset) { /* dataset1 of arity 1 with fixture */ }
	BOOST_DATA_TEST_CASE_F(fixture, test_case_name, dataset, var1) { /* dataset1 of arity 1 with fixture */ }
	BOOST_DATA_TEST_CASE_F(fixture, test_case_name, dataset, var1, ..., varN) { /* dataset1 of arity N with fixture */ }
	
	BOOST_AUTO_TEST_SUITE(test_suite, *decor1() *decor2())
	//test units
	BOOST_AUTO_TEST_SUITE_END()
	
	BOOST_FIXTURE_TEST_SUITE(test_suite, Fx, *decor1() *decor2())
	//test units
	BOOST_AUTO_TEST_SUITE_END()

	BOOST_TEST(statement); 等同于BOOST_TEST_CHECK
	BOOST_TEST_<level>(statement);
	BOOST_TEST_<level>(statement, optional_modifiers)
		"failure message"
		boost::test_tools::tolerance(0.001)
		boost::test_tools::bitwise()
		boost::test_tools::per_element()
		boost::test_tools::lexicographic()
	BOOST_<LEVEL>(predicate);
	BOOST_<LEVEL>_MESSAGE(predicate, message);
	BOOST_<LEVEL>_BITWISE_EQUAL(left, right);
	BOOST_<LEVEL>_<EQUAL/NE/GE/GT/LE/LT>(left, right);
	BOOST_<LEVEL>_EQUAL_COLLECTIONS(left_begin, left_end, right_begin, right_end);
	BOOST_<LEVEL>_CLOSE(left, right, tolerance);
	BOOST_<LEVEL>_CLOSE_FRACTION(left, right, tolerance);
	BOOST_<LEVEL>_EXCEPTION(expression, exception_type, predicate);
	BOOST_<LEVEL>_NO_THROW(expression);
	BOOST_<LEVEL>_THROW(expression, exception_type);
	BOOST_TEST_MESSAGE(test_message);
	BOOST_ERROR(message);
	BOOST_FAIL(message);
	BOOST_IS_DEFINED(symbol);

##装饰器
测试用例和测试套件可以添加装饰器：

	using namespace boost::unit_test;
	 
	expected_failures(counter_t number)
测试用例可以添加装饰器：

	using namespace boost::unit_test;
	
	timeout(unsigned seconds)
	
	template <typename FPT>
	  tolerance(FPT eps);
	template <typename FPT>
	  tolerance(fpc::percent_tolerance_t<FPT> eps) 

其他装饰器

	using namespace boost::unit_test;
	
	label(const_string l);
	description(const_string descr);
	depends_on(const_string dependency);
	template<bool condition> enable_if;
	enabled;
	disabled;
	fixture;
	precondition;
