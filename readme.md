# This repo is built for creating basic development environment for Linux.
# 1.bash 
1.1. edit the bash profile 
    
    vim ~/.bashrc
    
1.2. some alias

	alias ..='cd ..'
	alias ...='cd ../..'
	alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
	alias dir='dir --color=auto'
	alias egrep='egrep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias grep='grep --color=auto'
	alias info='info --vi-keys'
	alias l='ls -CF'
	alias la='ls -A'
	alias ll='ls -alF'
	alias ls='ls --color=auto'
	alias r='cd -'
	alias sc='snapcraft'
	alias vdir='vdir --color=auto'

1.3. dircolor
	
	git clone https://github.com/hhggit/dircolors-solarized.git
	wget https://raw.githubusercontent.com/hhggit/dircolors-solarized/master/dircolors.xterm-256color
	cd ~ && ln -sf [dircolors.xterm-256color] .dircolor
	
      
# 2. software
    sudo apt install g++ vim samba cmake python3-dev python-dev
## 2.1 vim
    wget -qO- https://raw.github.com/ma6174/vim/master/setup.sh | sh -x
## 2.2 samba

* share [homes]
 

```
#!bash

    smbpasswd -a username
    
    vim /etc/samba/smb.conf
```

	
* set selinux privilege for [homes]


```
#!bash

	setsebool -Psamba_enable_home_dirs on
	setsebool -Psamba_export_all_rw on
```

	
* allow samba pass the firewall

```
#!bash


	ufw allow samba
	
	iptables -I RH-Firewall-1-INPUT 5 -m state --state NEW -m tcp -p tcp --dport 139 -j ACCEPT
	iptables -I RH-Firewall-1-INPUT 5 -m state --state NEW -m tcp -p tcp --dport 445 -j ACCEPT
	iptables -I RH-Firewall-1-INPUT 5 -p udp -m udp --dport 137 -j ACCEPT
	iptables -I RH-Firewall-1-INPUT 5 -p udp -m udp --dport 138-j ACCEPT
	iptables-save
	service iptables  restart
```

	
* `service smb restart`
   
# 3. eclipse
1.download the latest eclipse C++ IDE from the [eclipse official site](http://www.eclipse.org/downloads/eclipse-packages)

[eclipse-cpp-neon-R-linux from USTC mirror](http://mirrors.ustc.edu.cn/eclipse/technology/epp/downloads/release/neon/R/eclipse-cpp-neon-R-linux-gtk-x86_64.tar.gz)

2.add plugins

* cmake editor <http://cmakeed.sourceforge.net/eclipse>
* python dev <http://pydev.org/update>

3.import eclipse profile
[eclipse.epf](eclipse.epf)

4.add c++14 support

* Global:
Windows>Preference>C/C++>Build>Settings>Discovery>CDT GCC Built-in Compliler Settings>Command to get compiler spec
_${COMMAND} ${FLAGS} -E -P -v _ `-std=c++14` _ -dD "${INPUTS}"_


* Project:
Properties>C/C++ General>Preprocessor Include>Providers>CDT GCC Built-in Compliler Settings>✅Use global provider shared between projects
