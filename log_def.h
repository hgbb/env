/*
 * log_def.h
 *
 *  Created on: 2014-8-6
 *      Author: HOiVG
 */

#ifndef LOG_DEF_H_
#define LOG_DEF_H_

#include <log4cplus/configurator.h>
#include <log4cplus/consoleappender.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#ifndef LOGGER
#define LOGGER (log4cplus::Logger::getRoot())
#endif  // LOGGER

#define LOG_PATTERN_DEFAULT "%D|%t|%-5p|%l|%m%n"

#define LOG_INIT_CONSOLE(strPattern)                                         \
  do {                                                                       \
    log4cplus::SharedAppenderPtr appender(new log4cplus::ConsoleAppender()); \
    std::auto_ptr<log4cplus::Layout> layout(                                 \
        new log4cplus::PatternLayout(strPattern));                           \
    appender->setLayout(layout);                                             \
    LOGGER.addAppender(appender);                                            \
  } while (0)

#define LOG_INIT_CONSOLE_DEFAULT() LOG_INIT_CONSOLE(LOG_PATTERN_DEFAULT)

#define LOG_TRACE(log) LOG4CPLUS_TRACE(LOGGER, log)

#define LOG_DEBUG(log) LOG4CPLUS_DEBUG(LOGGER, log)

#define LOG_INFO(log) LOG4CPLUS_INFO(LOGGER, log)

#define LOG_WARN(log) LOG4CPLUS_WARN(LOGGER, log)

#define LOG_ERROR(log) LOG4CPLUS_ERROR(LOGGER, log)

#define LOG_FATAL(log) LOG4CPLUS_FATAL(LOGGER, log)

#define LOG_LEVEL(level) LOGGER.setLogLevel(log4cplus::level##_LOG_LEVEL)

#define LOG_LEVEL_INT(level) \
  LOGGER.setLogLevel(level* log4cplus::DEBUG_LOG_LEVEL)

#define LOG_FUNC1(level) LOG4CPLUS_##level(LOGGER, __FUNCTION__)

#define LOG_FUNC(level, log) \
  LOG4CPLUS_##level(LOGGER, __FUNCTION__ << "|" << log)

#define LOG_TRACE_FUNC(log) LOG_FUNC(TRACE, log)
#define LOG_DEBUG_FUNC(log) LOG_FUNC(DEBUG, log)
#define LOG_INFO_FUNC(log) LOG_FUNC(INFO, log)
#define LOG_WARN_FUNC(log) LOG_FUNC(WARN, log)
#define LOG_ERROR_FUNC(log) LOG_FUNC(ERROR, log)
#define LOG_FATAL_FUNC(log) LOG_FUNC(FATAL, log)
#define LOG_TRACE_FUNC1() LOG_FUNC1(TRACE)
#define LOG_DEBUG_FUNC1() LOG_FUNC1(DEBUG)
#define LOG_INFO_FUNC1() LOG_FUNC1(INFO)
#define LOG_WARN_FUNC1() LOG_FUNC1(WARN)
#define LOG_ERROR_FUNC1() LOG_FUNC1(ERROR)
#define LOG_FATAL_FUNC1() LOG_FUNC1(FATAL)

#define LOG_FMT(level, ...) LOG4CPLUS_##level##_FMT(LOGGER, __VA_ARGS__)
#define LOG_TRACE_FMT(...) LOG_FMT(TRACE, __VA_ARGS__)
#define LOG_DEBUG_FMT(...) LOG_FMT(DEBUG, __VA_ARGS__)
#define LOG_INFO_FMT(...) LOG_FMT(INFO, __VA_ARGS__)
#define LOG_WARN_FMT(...) LOG_FMT(WARN, __VA_ARGS__)
#define LOG_ERROR_FMT(...) LOG_FMT(ERROR, __VA_ARGS__)
#define LOG_FATAL_FMT(...) LOG_FMT(FATAL, __VA_ARGS__)

#define LOG_TRACE_LIFE() LOG4CPLUS_TRACE_METHOD(LOGGER, __FUNCTION__)

#define LOG_ASSERT(...) LOG4CPLUS_ASSERT(LOGGER, __VA_ARGS__);

#endif /* LOG_DEF_H_ */
