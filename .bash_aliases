export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
alias ..="cd .."
alias ...="cd ../.."
alias r="cd -"
alias findc="find -type f -regex '.+\.\(h\|cpp\|hpp\|cxx\|c\|C\)'"
alias info="info --vi-keys"
man() {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
        LESS_TERMCAP_md=$'\E[01;38;5;74m' \
        LESS_TERMCAP_me=$'\E[0m' \
        LESS_TERMCAP_se=$'\E[0m' \
        LESS_TERMCAP_so=$'\E[1;7m' \
        LESS_TERMCAP_ue=$'\E[0m' \
        LESS_TERMCAP_us=$'\E[04;38;5;146m' \
        man "$@"
}

eval $(keychain --eval --quiet id_rsa)

screenfetch
