cmake_minimum_required(VERSION 3.0)

project(cmake_lists)

# shell: cmake -DCMAKE_BUILD_TYPE=DEBUG
#set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_FLAGS "-Wall")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
set(CMAKE_CXX_STANDARD_REQURED 14)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_EXTENSIONS OFF) #use -std=c++14 instead of -std=gnu++14

if(NOT DEFINED DOT_CPP)
    set(DOT_CPP ".cpp")
endif()

include_directories(/usr/local/include)

macro(add_exe exe)
    if(${ARGC} EQUAL 1)
         add_exe(${exe} "${exe}${DOT_CPP}")
    else()
        set(src_list_)
        
        foreach(path_ ${ARGN})
            get_filename_component(path_ ${path_} ABSOLUTE ) 
            if(NOT EXISTS ${path_})
                 message(WARNING "add_exe(${exe} ${ARGN}) # ${path_} : not exists")
            elseif(IS_DIRECTORY ${path_})
                aux_source_directory(${path_} path_list_)
                message(STATUS "add_exe(${exe} ${ARGN}) # AUX_SRC_DIR(${path_}) => ${path_list_}")
                list(APPEND src_list_ ${path_list_})
            else()
                list(APPEND src_list_ ${path_})
            endif()
        endforeach()
        
        if(NOT src_list_)
            message(SEND_ERROR "add_exe(${exe} ${ARGN}) ## no src found")
        else() 
            list(REMOVE_DUPLICATES src_list_)
            message(STATUS "add_exe(${exe} ${ARGN}) ## ${src_list_}") 
            add_executable(${exe} ${src_list_})
        endif()
    endif()
endmacro()


macro(add_exe_std std_number exe)
    add_exe(${exe} ${ARGN})
    set_property(TARGET ${exe} PROPERTY CXX_STANDARD ${std_number})
endmacro()

macro(add_exe11 exe)
    add_exe_std(11 ${exe} ${ARGN})
endmacro()

macro(add_exe14 exe)
    add_exe_std(14 ${exe} ${ARGN})
endmacro()

macro(add_exe98 exe)
    add_exe_std(98 ${exe} ${ARGN})
endmacro()

macro(add_boost_test test_module)
    add_exe(${test_module} ${ARGN})
    target_link_libraries(${test_module} boost_unit_test_framework.a)
endmacro()

macro(targert_link_boost_log targert_name)
    target_link_libraries(${targert_name} boost_log boost_thread boost_system pthread ${ARGN})
endmacro()


